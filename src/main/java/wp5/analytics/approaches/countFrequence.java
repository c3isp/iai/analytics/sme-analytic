package wp5.analytics.approaches;
import java.util.*; 

public class countFrequence {
	public static String countFrequencies(ArrayList<String> list) 
    { 
		String result = "";
        // hashmap to store the frequency of element 
        Map<String, Integer> hm = new HashMap<String, Integer>(); 
  
        for (String i : list) { 
            Integer j = hm.get(i); 
            hm.put(i, (j == null) ? 1 : j + 1); 
        } 
  
        // displaying the occurrence of elements in the arraylist 
        for (Map.Entry<String, Integer> val : hm.entrySet()) { 
            System.out.println("Element " + val.getKey() + " "
                               + "occurs"
                               + ": " + val.getValue() + " times");
            result = result + val.getKey() + " : " + val.getValue() + "\n";
        } 
        return result; 
    } 

}
