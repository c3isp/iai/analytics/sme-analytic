/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package wp5.analytics.restapi.impl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import wp5.analytics.connectors.ISIProxy;
import wp5.analytics.restapi.types.Structure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import wp5.analytics.approaches.countFrequence;
import wp5.analytics.connectors.FAProxy;

import com.github.jcustenborder.cef.CEFParserFactory;
import com.github.jcustenborder.cef.CEFParser;
import com.github.jcustenborder.cef.Message;


@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class SMEAnalyticsRESTAPI {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	@Value("${setting1}")
	private String setting1;

	@Value("${setting2}")
	private String setting2;
	
	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${rest.endpoint.url.isiapi}")
	private String isiAPIURL;
	@Value("${isiapi.readdpo.input_metadata}")
	private String input_metadata;
	@Value("${isiapi.security.user.name}")
	private String isiRestUser;
	@Value("${isiapi.security.user.password}")
	private String isiRestPassword;
	
	@Value("${rest.endpoint.url.fa}")
	private String faURL;	
	@Value("${fa.security.user.name}")
    private String faRestUser;
    @Value("${fa.security.user.password}")
    private String faRestPassword;
	
	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;
	
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(isiRestUser, isiRestPassword).build();
    }
	
	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger log = LoggerFactory.getLogger(SMEAnalyticsRESTAPI.class);

	
	
	/**
	 * runAnalytics
	 * @param serviceName
	 * 			the Analytics service name from the IAI API
	 * @param searchString
	 * 			correctly formatted JSON search string for retrieving the DPOs from DPOS
	 * @return the DPO id of the analytic result
	 * @throws JSONException 
	 * @throws IOException 
	 */
	@ApiOperation(notes = "run the collaborative on CTI data. The dpo search will depend on the search string. The input is a JSON string"
			+ " \n\n"
			+ "Example input: " 
			+ " \n\n"
			+ "	{ \n"  
			+ "		\"combining_rule\": \"and\",\n" 
			+ "		\"criteria\": [\n"  
			+ "		{\n"  
			+ "			\"attribute\": \"event_type\",\n"
			+ "			\"operator\": \"eq\",\n"  
			+ "			\"value\": \"firewall\"\n"   
			+"		}\n"  
			+"		]\n" 
			+"	}\n\n"
			+""
			, value = "run Analytics")
	@ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.POST, value="/findAttackingHosts/")
    public ResponseEntity<String> findAttackingHosts(@RequestBody @ApiParam(defaultValue="hello") String searchString ) throws JSONException, IOException {
		
		//String result = null;
		//HttpStatus httpstatus;
        //ResponseEntity<String> response = null;
		
        /*try{
  		   result = orchestratorObj.runAnalytics(serviceName, searchString);  		   
  		   if(result!=null){
			  if(!result.isEmpty()){
	 		      httpstatus = HttpStatus.OK;
	 		  }else{
	 			  httpstatus = HttpStatus.NOT_FOUND; 
	 		  }   
		   }else{
			  httpstatus = HttpStatus.EXPECTATION_FAILED;
		   }  		   
  		}catch (Exception e){
  			result = "Error running";
          	httpstatus = HttpStatus.BAD_REQUEST;
          	e.printStackTrace();
  		}
  		response = new ResponseEntity<String>(result, httpstatus);*/
  		
		//To get DPO IDs
		List<String> searchResult = new ArrayList<String>();
		HttpStatus httpstatus;
        //ResponseEntity<List<String>> response = null;
        ResponseEntity<String> response = null;
        ISIProxy isiProxy = new ISIProxy(isiAPIURL,isiRestUser,isiRestPassword);
		try{
		   searchResult = isiProxy.searchDPO(searchString, false);
		   if(!searchResult.isEmpty()){
		      httpstatus = HttpStatus.OK;
		   }else{
			   httpstatus = HttpStatus.NOT_FOUND; 
		   }
		}catch (Exception e){
			searchResult = new ArrayList<String>();
        	searchResult.add("Error running");
        	httpstatus = HttpStatus.BAD_REQUEST;
        	e.printStackTrace();
		}
		//response = new ResponseEntity<List<String>>(searchResult, httpstatus);
		
		//To get STIX DPO
		String dpoID = null;
		String dpoSTIX = null;
		String dpoCEF = "";
		FAProxy faRestClient = new FAProxy(faURL, faRestUser, faRestPassword);
		for(int i = 0; i < searchResult.size(); i++){
			dpoID = searchResult.get(i);
			dpoSTIX = isiProxy.readDPO(dpoID, input_metadata);
			dpoCEF = dpoCEF + faRestClient.removeSTIXLayer(dpoSTIX)+ "\n";
		}
		//response = new ResponseEntity<String>(dpoCEF, httpstatus);
		
		//Parse CEF
		List<String> dpoCEF_list = Arrays.asList(dpoCEF.split("\n"));
		ArrayList<String> src_list = new ArrayList<String>(); 
		CEFParser f = CEFParserFactory.create();
		for(int i = 0; i < dpoCEF_list.size(); i++ ){
			//Message message = f.parse("CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 14:25:10 start=1528637110383 dvchost=cscloud2c9a.kent.ac.uk reason=Dropped Retransmit TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=TCP cs2=ACK src=2400:cb00:2048:1:0:0:6810:bd8a smac=F8:66:F2:43:82:46 spt=80 dst=2001:630:340:1103:f816:3eff:fe68:2d73 dmac=FA:16:3E:68:2D:73 dpt=47356 out=1294 cnt=1 end=null cs3=Connection Flow cs4=139 msg=\"rxjammed\" cs5=ACK cs6=38105 TrendMicroDsPacketData=[B@1dc08994 cat=AGENT");
			Message message = f.parse(dpoCEF_list.get(i));
			String src = message.extensions().get("src");
			src_list.add(src);
			log.info("src = " + src + "\n");
		}
	    
	    //sort the src list
		Collections.sort(src_list);
		System.out.println("sort out: "+src_list);
		Set<String> distinct = new HashSet<>(src_list);
		
		String analysis_result = "";
		for (String s: distinct) {
			System.out.println(s + ": " + Collections.frequency(src_list, s));
			analysis_result = analysis_result + s + ": " + Collections.frequency(src_list, s) + "\n";
		}
        //String rst = countFrequence.countFrequencies(src_list);
		/*Map<String, Long> couterMap = src_list.stream().collect(Collectors.groupingBy(e -> e.toString(),Collectors.counting()));
        System.out.println("!!the result: "+couterMap);*/

		response = new ResponseEntity<String>(analysis_result, httpstatus);
		return response;
		
	}
	
	
	
	
	/**
	 * Sample POST method
	 * 
	 * @param pathParam
	 *            a parameter extracted from path like /template/PARAM_VALUE/ it
	 *            has to match the {param} from the path
	 * @param headerdata
	 *            extracted from the path
	 * @param data
	 *            is extracted from the request payload
	 * @return just a string
	 *//*
	@RequestMapping(method = RequestMethod.POST, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@PathVariable("param") String pathParam,
			@RequestHeader(value = "template-header-param") String headerdata, @RequestBody() Structure data) {
		LOGGER.info("I received a POST request");
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}

	*//**
	 * Sample PUT method
	 *//*
	@RequestMapping(method = RequestMethod.PUT, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@PathVariable("param") String pathParam,
			@RequestHeader(value = "template-header-param") String headerdata, @RequestBody() Structure data) {
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}

	*//**
	 * an example of GET, that returns a json based on the Structure class.
	 * SpringBoot takes care of the transformation of the returned type. The
	 * interface tells that it returns a json via produces =
	 * MediaType.APPLICATION_JSON_VALUE
	 * ApiImplicitParams are useful to document implicit parameters like HTTP
	 * headers that are not in the method signature
	 *//*
	@ApiOperation(httpMethod = "GET", value = "Get the resource",
			notes = "Return a json based on the Structure class. SpringBoot takes care of the transformation of the returned type the interface")
	@RequestMapping(method = RequestMethod.GET, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "X-myheader-attr1", value = "X-myheader-attr1", required = false, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "X-myheader-attr2", value = "X-myheader-attr2", required = false, dataType = "string", paramType = "header")
      })
	public ResponseEntity<Structure> get(@PathVariable("param") String param, HttpServletRequest hreq, Authentication authentication) {
	    
	    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
	    
		Structure responsedata = new Structure();
		responsedata.setId("anIdValue" + ", Principal name=" + userDetails.getUsername());
		responsedata.setName("aName");
		responsedata.setPath("aPath");
		responsedata.setVersion("1");

		return new ResponseEntity<Structure>(responsedata, HttpStatus.OK);
	}

	*//**
	 * an example of DELETE
	 *//*
	@ApiOperation(httpMethod = "DELETE", value = "Delete the resource")
	@RequestMapping(method = RequestMethod.DELETE, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@PathVariable("object") String objectId) {
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}

	*//**
	 * an example of HEAD REST API that leverage the ApiOperation, ApiResponse
	 * annotations that are used by swagger to add some description to the
	 * current operation when it generates the swagger-ui.html file. Notice that
	 * HEAD is not supported and while it will appear in the docs, it won't be
	 * possible to 'try it out'
	 *//*
	// provides a description
	@ApiOperation(httpMethod = "HEAD", value = "Head the resource", notes = "Return the metadata associated with an object")
	// provides a documentation of the different http error messages and their
	// meaning from the application perspective
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Client Error"),
			@ApiResponse(code = 404, message = "Param not found"),
			@ApiResponse(code = 200, message = "Returned some headers") })
	@RequestMapping(method = RequestMethod.HEAD, value = "/template/{param}/")
	public ResponseEntity<String> head(@PathVariable("param") String param) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("template-header-1", setting1);
		responseHeaders.set("template-header-2", setting2);
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(responseHeaders, HttpStatus.OK);
		return thisresponse;
	}
	
	*//**
	 * A simple GET REST endpoint that calls another REST endpoint
	 * This shows how to use RestTemplate class to issue REST calls
	 *//*
	@ApiOperation(httpMethod = "GET", value = "Call Get method",
			notes = "See Get method")
	@RequestMapping(method = RequestMethod.GET, value = "/calltemplate/{param}/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Structure> callGet(@PathVariable("param") String param, HttpServletRequest hreq) {
	
		MultiValueMap<String, String> mapParams = new LinkedMultiValueMap<String, String>();
		mapParams.add("param", param);
		Structure structure = restTemplate.getForObject(callGetEndpoint, Structure.class, mapParams);
		
		ResponseEntity<Structure> thisresponse = new ResponseEntity<Structure>(structure, HttpStatus.OK);
		return thisresponse;
	}*/
}
