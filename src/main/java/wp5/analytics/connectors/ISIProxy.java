package wp5.analytics.connectors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import wp5.analytics.constants.JSONCreator;

@Component
public class ISIProxy {	
	
    /**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	
	/*@Value("${rest.endpoint.url.isiapi}")
	private String isiapiURL;
	
	@Value("${isiapi.security.user.name}")
    private String restUser;
    @Value("${isiapi.security.user.password}")
    private String restPassword;
    
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }*/
    
	private static final Logger log = LoggerFactory.getLogger(ISIProxy.class);
	
	private String isiapiURL;
    private String restUser;
    private String restPassword;
    private RestTemplate restTemplate;
    
    public ISIProxy(){
    	
    }
    
	/**
	 * ISIProxy
	 * @param isiapiURL
	 *        ISI API REST endpoint URL 
	 * @param restUser
	 * 		  the REST user going to access the ISI API	
	 * @param restPassword
	 * 		  the REST user's password
	 */
	public ISIProxy(String isiapiURL, String restUser, String restPassword){
		this.isiapiURL = isiapiURL;
		this.restUser = restUser;
		this.restPassword = restPassword;
		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
		this.restTemplate=restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
		//SSLCertificateValidation.disable();
	}
	
    /**
     * searchDSA
     * @param searchString
     * 			correctly formatted JSON search string
     * @param longResultFlag
     * 			if true, returns an array of metadata entries; if false, returns an array of dsa_ids
     * @return	DSA metadata of DSAs matching the search parameters. If longResultFlag is true, returns all metadata for each entry; if it's false, returns the DSA IDs.
     * @throws JSONException
     */
    public List<String> searchDPO(String searchString, boolean longResultFlag) throws JSONException{		
		List<String> searchResult = new ArrayList<String>();
		JSONCreator jsonCreator = new JSONCreator();
		String searchString_dpo = jsonCreator.forSearchDSA(searchString).toString();
		String url = isiapiURL + "/search/{store}/{longResultFlag}";
				
		//MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        //parameters.add("params", searchString_dsa);
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(searchString_dpo, headers);
        ResponseEntity<List> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, List.class, "dpos", longResultFlag);
        
		if(response.getStatusCode()==HttpStatus.OK){
			searchResult = response.getBody();
        	log.info("return string: " + searchResult);
        }
		
		return searchResult;
	}
	
	/**
	 * createDPO
	 * @param ctiFilePath
	 * 			CTI file (containing the CTI data) path
	 * @param ctiMetadata
	 * 			CTI metadata string
	 * @return DPO id
	 * @throws IOException
	 * @throws JSONException
	 */
	public String createDPO (String ctiFilePath, String ctiMetadata, String norm) throws IOException, JSONException{
		String ctiFileDir ="./" + ctiFilePath;
		File ctiFile = null;
		try{
		   ctiFile = new File(ctiFileDir);
		}catch(Exception e){
			log.info("cti file error.");
		}
				
        Resource resource_cti = new FileSystemResource(ctiFile);
        
        log.info("resource cti: " +  resource_cti.toString());
        
        String url = isiapiURL + "/dpo/";
        log.info("isi api create dpo url: " +  url);
        //System.out.println(url);
        log.info("user/password:" +  restUser + "/" + restPassword);
        log.info("RestTemplate" +  restTemplate.toString());
          
        JSONCreator jsonCreator = new JSONCreator();
        String input_metadata = jsonCreator.forCreateDPO(ctiMetadata).toString();
        
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        parameters.add("input_metadata", input_metadata);
        parameters.add("norm", norm);
        parameters.add("fileToSubmit", resource_cti);        
        log.info("data normorization: " +  norm);
        //using restTemplate exchange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(parameters, headers);
        ResponseEntity<String> response = null;
        
        String dpoID = null;
        try{
        	response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        	if(response.getStatusCode()==HttpStatus.OK){
         	   dpoID = parseReturn(response.getBody().toString());        	 
         	   log.info("return dpo id: " + dpoID);
         	   log.info("the input metadata: " +  input_metadata);
            }
        }catch(Exception e){
        	//re_str = e.getMessage();
        	log.info("exception: " +  e.getMessage());        	
        }                        
        
        return dpoID;
	}
	
	/**
	 * parseReturn
	 * @param result
	 * 			the return from ISI API for creating DPO
	 * @return DPO id
	 * @throws JSONException
	 */
	public String parseReturn(String result) throws JSONException{
		JSONObject jsonObj_rt = new JSONObject(result);
		JSONObject jsonObj_addiotnalProperties = jsonObj_rt.getJSONObject("additionalProperties"); 
    	String dpoID = jsonObj_addiotnalProperties.getString("dposId");		
		return dpoID;
		
	}
	
	
	/**
	 * readDPO
	 * @param DPO id for searching the DPO data
	 * @return string containing the DPO data entries
	 * @throws JSONException
	 */
	public String readDPO(String dpoID, String input_metadata) throws JSONException{
		JSONCreator jsonCreator = new JSONCreator();
		//String input_metadata = jsonCreator.forReadDPO(dpoID).toString();
		String url = isiapiURL + "/dpo/{dpo_id}/";
				
		//MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        //parameters.add("X-c3isp-input_metadata", input_metadata);
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-c3isp-input_metadata", input_metadata);
        
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        //log.info("the metadata: " +  input_metadata);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class, dpoID);
        //ResponseEntity<String> response = restTemplate.getForEntity(url, String.class, dpo_id);
        log.info("the dpo metadata: " +  input_metadata);
        String preview_DPO = null;
        
        if(response.getStatusCode()==HttpStatus.OK){
        	preview_DPO = response.getBody().toString();
        	log.info("return string: " + preview_DPO);
        }
        //Stub
        /*preview_DPO = "CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 14:25:10 start=1528637110170 dvchost=cscloud2c9a.kent.ac.uk reason=Dropped Retransmit TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=TCP cs2=ACK src=2400:cb00:2048:1:0:0:6810:bd8a smac=F8:66:F2:43:82:46 spt=80 dst=2001:630:340:1103:f816:3eff:fe68:2d73 dmac=FA:16:3E:68:2D:73 dpt=47356 out=1294 cnt=1 end=null cs3=Connection Flow cs4=139 msg=\"rxjammed\" cs5=ACK cs6=38104 TrendMicroDsPacketData=[B@4870446b cat=AGENT\n" 
        		+"CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 14:25:10 start=1528637110383 dvchost=cscloud2c9a.kent.ac.uk reason=Dropped Retransmit TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=TCP cs2=ACK src=2400:cb00:2048:1:0:0:6810:bd8a smac=F8:66:F2:43:82:46 spt=80 dst=2001:630:340:1103:f816:3eff:fe68:2d73 dmac=FA:16:3E:68:2D:73 dpt=47356 out=1294 cnt=1 end=null cs3=Connection Flow cs4=139 msg=\"rxjammed\" cs5=ACK cs6=38105 TrendMicroDsPacketData=[B@1cf79ea2 cat=AGENT\n"
        		+"CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 17:31:36 start=1528648296950 dvchost=cscloud2c9a.kent.ac.uk reason=Invalid IPv6 Address TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=Other: 0 cs2=\"\" src=0:0:0:0:0:0:0:0 smac=FA:16:3E:20:00:17 spt=0 dst=ff02:0:0:0:0:0:0:16 dmac=33:33:00:00:00:16 dpt=0 out=90 cnt=1 end=null cs3=Connection Flow cs4=147 msg=\"\" cs5=\"\" cs6=38106 TrendMicroDsPacketData=[B@1513d37c cat=AGENT\n"
                +"CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 17:31:37 start=1528648297070 dvchost=cscloud2c9a.kent.ac.uk reason=Invalid IPv6 Address TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=Other: 0 cs2=\"\" src=0:0:0:0:0:0:0:0 smac=FA:16:3E:20:00:17 spt=0 dst=ff02:0:0:0:0:0:0:16 dmac=33:33:00:00:00:16 dpt=0 out=90 cnt=1 end=null cs3=Connection Flow cs4=147 msg=\"\" cs5=\"\" cs6=38107 TrendMicroDsPacketData=[B@2fc4c124 cat=AGENT";*/
        /*preview_DPO ="CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 14:25:10 start=1528637110170 dvchost=cscloud2c9a.kent.ac.uk reason=Dropped Retransmit TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=TCP cs2=ACK src=2400:cb00:2048:1:0:0:6810:bd8a smac=F8:66:F2:43:82:46 spt=80 dst=2001:630:340:1103:f816:3eff:fe68:2d73 dmac=FA:16:3E:68:2D:73 dpt=47356 out=1294 cnt=1 end=null cs3=Connection Flow cs4=139 msg=\"rxjammed\" cs5=ACK cs6=38104 TrendMicroDsPacketData=[B@17d0284a cat=AGENT\n"
                +"CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 14:25:10 start=1528637110383 dvchost=cscloud2c9a.kent.ac.uk reason=Dropped Retransmit TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=TCP cs2=ACK src=2400:cb00:2048:1:0:0:6810:bd8a smac=F8:66:F2:43:82:46 spt=80 dst=2001:630:340:1103:f816:3eff:fe68:2d73 dmac=FA:16:3E:68:2D:73 dpt=47356 out=1294 cnt=1 end=null cs3=Connection Flow cs4=139 msg=\"rxjammed\" cs5=ACK cs6=38105 TrendMicroDsPacketData=[B@1dc08994 cat=AGENT\n"
        		+"CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 17:31:36 start=1528648296950 dvchost=cscloud2c9a.kent.ac.uk reason=Invalid IPv6 Address TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=Other: 0 cs2=\"\" src=0:0:0:0:0:0:0:0 smac=FA:16:3E:20:00:17 spt=0 dst=ff02:0:0:0:0:0:0:16 dmac=33:33:00:00:00:16 dpt=0 out=90 cnt=1 end=null cs3=Connection Flow cs4=147 msg=\"\" cs5=\"\" cs6=38106 TrendMicroDsPacketData=[B@782fe438 cat=AGENT\n"
                + "CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|rt=Jun 10 2018 17:31:37 start=1528648297070 dvchost=cscloud2c9a.kent.ac.uk reason=Invalid IPv6 Address TRendMicroDs Tags= \"\" act=Deny cs1=50 deviceDirecttion=Incoming deviceInboundInterface=FA:16:3E:68:2D:73 TrendMicroDsFrameType=IPv6 proto=Other: 0 cs2=\"\" src=0:0:0:0:0:0:0:0 smac=FA:16:3E:20:00:17 spt=0 dst=ff02:0:0:0:0:0:0:16 dmac=33:33:00:00:00:16 dpt=0 out=90 cnt=1 end=null cs3=Connection Flow cs4=147 msg=\"\" cs5=\"\" cs6=38107 TrendMicroDsPacketData=[B@1636655f cat=AGENT";*/
        
		return preview_DPO;
	}
	
	/**
	 * deleteDPO
	 * @param dpoID
	 * 		DPO id
	 * @return a string indicating the delete succeed or not
	 * @throws JSONException
	 */
	public String deleteDPO(String dpoID) throws JSONException{
		JSONCreator jsonCreator = new JSONCreator();
		String input_metadata = jsonCreator.forDeleteDPO(dpoID).toString();
		
		String url = isiapiURL + "/dpo/{dpo_id}/";
		
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(input_metadata, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, String.class, dpoID);
		
		String result = null;
		if(response.getStatusCode()==HttpStatus.OK){
			result = "dpo delete success";
			log.info("delete succeed: " + dpoID);
        }else{
        	result = "dpo delete fail";
        	log.info("delete fail: " + dpoID);
        }
		
		return result;
	}
}
