package wp5.analytics.connectors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class FAProxy {	
	
    /**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	
	/*@Value("${rest.endpoint.url.isiapi}")
	private String isiapiURL;
	
	@Value("${isiapi.security.user.name}")
    private String restUser;
    @Value("${isiapi.security.user.password}")
    private String restPassword;
    
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }*/
    
	private static final Logger log = LoggerFactory.getLogger(FAProxy.class);
	
	private String faURL;
    private String restUser;
    private String restPassword;
    private RestTemplate restTemplate;
    
    public FAProxy(){
    	
    }
    
	/**
	 * FAProxy
	 * @param faURL
	 *        FA endpoint URL 
	 * @param restUser
	 * 		  the REST user going to access the FA API	
	 * @param restPassword
	 * 		  the REST user's password
	 */
	public FAProxy(String faURL, String restUser, String restPassword){
		this.faURL = faURL;
		this.restUser = restUser;
		this.restPassword = restPassword;
		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
		this.restTemplate=restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
		//SSLCertificateValidation.disable();
	}
	
	/**
	 * removeSTIX:ayer
	 * @param dpo
	 * 			dpo with STIX Layer string
	 * @return dpo without STIX Layer
	 * @throws IOException
	 * @throws JSONException
	 */
	public String removeSTIXLayer (String dpo) throws IOException, JSONException{
		String dpoFileDir ="./dpoSTIX.txt";
		File dpoFile = null;
		try{
		   dpoFile = new File(dpoFileDir);
		   if(!dpoFile.exists()){
			   dpoFile.createNewFile();
		    }else{
		    	dpoFile.delete();
		    	try {
		    		dpoFile.createNewFile();
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    	
		    }
		    FileWriter fw = new FileWriter(dpoFile,true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(dpo);
			bw.close();
			fw.close();
		}catch(Exception e){
			log.info("dpo file error.");
		}
		
		
		
        Resource resource_dpo = new FileSystemResource(dpoFile);
        
        log.info("resource dpo: " +  resource_dpo.toString());
        
        String url = faURL + "/convertDL/";
        log.info("fa remove STIX Layer url: " +  url);
        //System.out.println(url);
        log.info("user/password:" +  restUser + "/" + restPassword);
        log.info("RestTemplate" +  restTemplate.toString());
                 
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        parameters.add("file", resource_dpo);        
 
        //using restTemplate exchange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(parameters, headers);
        ResponseEntity<String> response = null;
        
        String dpoCEF = null;
        try{
        	response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        	if(response.getStatusCode()==HttpStatus.OK){
         	   dpoCEF = response.getBody().toString();        	 
         	   log.info("return CEF dpo: successful");
            }
        }catch(Exception e){
        	//re_str = e.getMessage();
        	log.info("exception: " +  e.getMessage());        	
        }                        
        
        return dpoCEF;
	}
	
}
