package wp5.analytics.constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONCreator {
	
	/**
	 * forSearchDSA
	 * @param searchString
	 * 			correctly formatted JSON search string
	 * @return	the JSON Object requested
	 * @throws JSONException
	 */
	public JSONObject forSearchDSA(String searchString) throws JSONException{
		
		JSONObject jsonObj_payload = new JSONObject();
		jsonObj_payload.put("AttributeId","ns:c3isp:search-string");
		jsonObj_payload.put("Value",searchString);
		jsonObj_payload.put("DataType","string");
		JSONArray array = new JSONArray();
        array.put(jsonObj_payload);
        JSONObject jsonObj_attribute = new JSONObject();
        jsonObj_attribute.put("Attribute", array);
        JSONObject jsonObj_request = new JSONObject();
        jsonObj_request.put("Request", jsonObj_attribute);
		
		return jsonObj_request;
	}
	
    /**
     * forCreateDPO
     * @param ctiMetadata
     * 			the CTI metadata string
     * @return  the JSON Object requested
     * @throws JSONException
     */
    public JSONObject forCreateDPO(String ctiMetadata) throws JSONException{
    	JSONObject jsonObj = new JSONObject(ctiMetadata);
		
    	JSONObject jsonObj_payload_dsaID = new JSONObject();
		jsonObj_payload_dsaID.put("AttributeId","ns:c3isp:dsa-id");
		jsonObj_payload_dsaID.put("Value",jsonObj.getString("dsa_id"));
		jsonObj_payload_dsaID.put("DataType","string");
		
		JSONObject jsonObj_payload_dpoMetadata = new JSONObject();
		jsonObj_payload_dpoMetadata.put("AttributeId","ns:c3isp:dpo-metadata");
		jsonObj_payload_dpoMetadata.put("Value", ctiMetadata);
		jsonObj_payload_dpoMetadata.put("DataType","string");
		
		JSONArray array = new JSONArray();
        array.put(jsonObj_payload_dsaID);
        array.put(jsonObj_payload_dpoMetadata);
        
        JSONObject jsonObj_attribute = new JSONObject();
        jsonObj_attribute.put("Attribute", array);
        JSONObject jsonObj_request = new JSONObject();
        jsonObj_request.put("Request", jsonObj_attribute);
		
		return jsonObj_request;
	}
    
    /**
     * forReadDPO
     * @param dpoID
     * 			the DPO id
     * @return  the JSON Object requested
     * @throws JSONException
     */
    public JSONObject forReadDPO(String dpoID) throws JSONException{
		
    	JSONObject jsonObj_payload_dpoID = new JSONObject();
		jsonObj_payload_dpoID.put("id",dpoID);
		
		JSONObject jsonObj_payload_dpoMetadata = new JSONObject();
		jsonObj_payload_dpoMetadata.put("AttributeId","ns:c3isp:dpo-metadata");
		jsonObj_payload_dpoMetadata.put("Value", jsonObj_payload_dpoID.toString());
		jsonObj_payload_dpoMetadata.put("DataType","string");
		
		JSONArray array = new JSONArray();
        array.put(jsonObj_payload_dpoMetadata);
        
        JSONObject jsonObj_attribute = new JSONObject();
        jsonObj_attribute.put("Attribute", array);
        JSONObject jsonObj_request = new JSONObject();
        jsonObj_request.put("Request", jsonObj_attribute);
		
		return jsonObj_request;
	}
    
    /**
     * forDeleteDPO
     * @param dpoID
     * 			the DPO id
     * @return
     * 			the JSON Object requested
     * @throws JSONException
     */
    public JSONObject forDeleteDPO(String dpoID) throws JSONException{
		
    	//JSONObject jsonObj_payload_dpoID = new JSONObject();
		//jsonObj_payload_dpoID.put("id",dpo_id);
		
		JSONObject jsonObj_payload_dpoMetadata = new JSONObject();
		jsonObj_payload_dpoMetadata.put("AttributeId","ns:c3isp:dpo-metadata");
		jsonObj_payload_dpoMetadata.put("Value", dpoID);
		jsonObj_payload_dpoMetadata.put("DataType","string");
		
		JSONArray array = new JSONArray();
        array.put(jsonObj_payload_dpoMetadata);
        
        JSONObject jsonObj_attribute = new JSONObject();
        jsonObj_attribute.put("Attribute", array);
        JSONObject jsonObj_request = new JSONObject();
        jsonObj_request.put("Request", jsonObj_attribute);
		
		return jsonObj_request;
	}

}
